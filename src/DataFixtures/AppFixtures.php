<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
         $product = new Product();
         $product->setPrice(79);
         $product->setBruttoPrice(100);
         $product->setName('Fixture product');
         $manager->persist($product);
         $manager->flush();
    }
}
