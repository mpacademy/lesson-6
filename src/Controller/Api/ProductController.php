<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13/05/2018
 * Time: 22:46
 */

namespace App\Controller\Api;

use App\Entity\Product;
use App\Form\Api\ProductType;
use App\Service\ProductService;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductController
 * @package App\Controller\Api
 * @Route("/api/product")
 */
class ProductController extends FOSRestController
{
    /**
     * @Rest\Get("/")
     * @SWG\Get(
     *     summary="Lists all products",
     *     tags={"Product"},
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully returned the list of products",
     *         @SWG\Schema(
     *              type="array",
     *              @Model(type=Product::class, groups={"api"})
     *         )
     *     )
     * )
     */
    public function productsAction()
    {
        $products = $this->getDoctrine()->getRepository('App:Product')->findAll();
        $context = new Context();
        $context->setGroups(['api']);
        $view = $this->view($products, Response::HTTP_OK)->setContext($context);
        return $this->handleView($view);
    }

    /**
     * @Rest\Get("/{id}")
     * @SWG\Get(
     *     summary="Product",
     *     tags={"Product"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully returned the product",
     *         @Model(type=Product::class, groups={"api"})
     *     )
     * )
     * @param Product $product
     * @return Response
     */
    public function productAction(Product $product)
    {
        $context = new Context();
        $context->setGroups(['api']);
        $view = $this->view($product, Response::HTTP_OK)->setContext($context);
        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/")
     * @SWG\Post(
     *     summary="New product",
     *     tags={"Product"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="Test product"),
     *              @SWG\Property(property="bruttoPrice", type="float", example="10.99")
     *         )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully returned the stored product",
     *         @Model(type=Product::class, groups={"api"})
     *     )
     * )
     * @param Request $request
     * @param ProductService $service
     * @return Response
     */
    public function newProductAction(Request $request, ProductService $service)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $service->calcNettoPrice($product);
            $this->getDoctrine()->getManager()->persist($product);
            $this->getDoctrine()->getManager()->flush();
            $context = new Context();
            $context->setGroups(['api']);
            $view = $this->view($product, Response::HTTP_OK)->setContext($context);
            return $this->handleView($view);
        }
        $view = $this->view([], Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * @Rest\Patch("/{id}")
     *
     * @SWG\Patch(
     *     summary="Edit product",
     *     tags={"Product"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="Test product"),
     *              @SWG\Property(property="bruttoPrice", type="float", example="10.99")
     *         )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully returned the edited product",
     *         @Model(type=Product::class, groups={"api"})
     *     )
     * )
     * @param Request $request
     * @param Product $product
     * @param ProductService $service
     * @return Response
     */
    public function editProductAction(Request $request, Product $product, ProductService $service)
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $service->calcNettoPrice($product);
            $this->getDoctrine()->getManager()->persist($product);
            $this->getDoctrine()->getManager()->flush();
            $context = new Context();
            $context->setGroups(['api']);
            $view = $this->view($product, Response::HTTP_OK)->setContext($context);
            return $this->handleView($view);
        }
        $view = $this->view([], Response::HTTP_OK);
        return $this->handleView($view);
    }


    /**
     * @Rest\Delete("/{id}")
     * @SWG\Delete(
     *     summary="Delete product",
     *     tags={"Product"},
     *     @SWG\Response(
     *         response=200,
     *         description="Successfully returned the list of products",
     *         @SWG\Schema(
     *              type="array",
     *              @Model(type=Product::class, groups={"api"})
     *         )
     *     )
     * )
     * @param Product $product
     * @return Response
     */
    public function deleteAction(Product $product)
    {
        $this->getDoctrine()->getManager()->remove($product);
        $this->getDoctrine()->getManager()->flush();
        $products = $this->getDoctrine()->getRepository('App:Product')->findAll();
        $context = new Context();
        $context->setGroups(['api']);
        $view = $this->view($products, Response::HTTP_OK)->setContext($context);
        return $this->handleView($view);
    }
}
