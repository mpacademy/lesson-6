<?php

namespace App\Service;

use App\Entity\Product;

class ProductService
{
    const VAT = 21;

    /**
     * @param Product $product
     */
    public function calcNettoPrice(Product $product)
    {
        if ($product->getBruttoPrice() == null) {
            return;
        }

        $product->setPrice($product->getBruttoPrice()-($product->getBruttoPrice()*(self::VAT/100)));
    }
}
