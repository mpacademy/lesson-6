<?php

namespace App\Service;

interface AppServiceInterface
{
    /**
     * @param bool $storeToFile
     * @return float
     */
    public function phpExecutionTime($storeToFile = false);

    /**
     * @param bool $storeToFile
     * @return float
     */
    public function inputOutputTime($storeToFile = false);

    /**
     * @param bool $storeToFile
     * @return float
     */
    public function networkTime($storeToFile = false);

    /**
     * Nothing return
     */
    public function deleteFiles();

}