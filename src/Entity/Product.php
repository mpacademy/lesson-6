<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"api"})
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=128)
     * @Groups({"api"})
     */
    private $name;

    /**
     * @var float|null
     * @ORM\Column(type="decimal", precision=14, scale=2, nullable=true)
     * @Groups({"api"})
     */

    private $price;

    /**
     * @var float|null
     * @ORM\Column(type="decimal", precision=14, scale=2, nullable=true)
     * @Groups({"api"})
     */
    private $bruttoPrice;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     * @return $this
     */
    public function setName(?string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     * @return $this
     */
    public function setPrice(?float $price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getBruttoPrice(): ?float
    {
        return $this->bruttoPrice;
    }

    /**
     * @param float|null $bruttoPrice
     * @return $this
     */
    public function setBruttoPrice(?float $bruttoPrice)
    {
        $this->bruttoPrice = $bruttoPrice;

        return $this;
    }
}
