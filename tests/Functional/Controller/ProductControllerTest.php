<?php


namespace App\Tests\Functional\Controller;

use App\Entity\Product;
use App\Tests\DataFixtures\AppFixtures;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ProductControllerTest extends WebTestCase
{

    /**
     * @return array
     */
    public function getNewData()
    {
        $out = [];
        $product = new Product();
        $productName = 'Test';
        $product->setId(2)->setName($productName)->setBruttoPrice(120)->setPrice(94.8);
        $out[] = [
            $product,
            'Test',
            '120'
        ];
        return $out;
    }

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        $application = new Application($kernel);
        $application->setAutoExit(false);
        try {
            $application->run(new StringInput('doctrine:database:drop --if-exists --quiet --quiet'));
            $application->run(new StringInput('doctrine:database:create --if-not-exists --quiet'));
            $application->run(new StringInput('doctrine:schema:update --force --quiet'));
        } catch (\Exception $exception) {
            var_dump($exception->getMessage());
        }

        $entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;

        $loader = new Loader();
        $loader->addFixture(new AppFixtures());

        $purger = new ORMPurger($entityManager);
        $executor = new ORMExecutor($entityManager, $purger);
        $executor->execute($loader->getFixtures());
    }

    /**
     * @dataProvider getNewData
     * @param $expected
     * @param string $name
     * @param float $bruttoPrice
     */
    public function testNew($expected, $name, $bruttoPrice)
    {
        $client = static::createClient();
        $crawler = $client->request(
            'GET',
            $client->getContainer()->get('router')->generate('product_new')
        );
        $form = $crawler->selectButton('product[save]')->form();
        $form['product[name]'] = $name;
        $form['product[bruttoPrice]'] = $bruttoPrice;
        $client->submit($form);
        $this->assertTrue($client->getResponse() instanceof RedirectResponse);
        $doctrine = $client->getContainer()->get('doctrine');
        $product = $doctrine->getManager()->getRepository('App:Product')->findOneBy(['name' => $name]);
        $this->assertEquals($expected, $product);
    }

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        $application = new Application($kernel);
        $application->setAutoExit(false);
        try {
            $application->run(new StringInput('doctrine:database:drop --force'));
        } catch (\Exception $exception) {
            var_dump($exception->getMessage());
        }
    }
}
